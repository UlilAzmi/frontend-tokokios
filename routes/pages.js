const express = require('express');
// const User = require('../core/user');
const utilSim = require('../core/utilSim');
var http = require('http');
const router = express.Router();

router.get('/', (req, res, next) => {
    res.render('index', {title: "Home Page"})
});

router.get('/data',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/homepage/productSmart?host=' + req.query.host, req, res, function(result){
        if(result.rc == "00"){
            res.json({success :true, data: result.object})
        }else{
            res.json({success :false, message: result.rd})
        }
    })
});

router.get('/group', (req, res, next) => {
    let cari = req.query.cari !== 'undefined'? req.query.cari : '';
    utilSim.httpSimGet('/homepage/productSmart?host=' +req.query.host + "&groupId="+ req.query.id + "&cari="+ cari, req, res, function(result){
        res.json({success :true, data: result.object})
    })
});

router.get('/kota', (req, res, next) => {
    utilSim.httpSimGet('/homepage/getKota?kata=' +req.query.kata, req, res, function(result){
        res.json({success :true, data: result.object})
    })
});

router.get('/ongkir', (req, res, next) => {
    utilSim.httpSimGet('/homepage/getOngkir?origin='+req.query.origin+'&destination='+req.query.destination
    +'&weight=' +req.query.weight, req, res, function(result){
        res.json({success :true, data: result.data})
    })
});

router.get('/metodeBayar', (req, res, next) => {
    utilSim.httpSimGet('/systems/metodeBayar/type/' + req.query.id, req, res, function(result){
        res.json({success :true, data: result.data})
    })
});

router.get('/about', (req, res) => {
    res.render('admin/about', {title: "About Me"})
});

router.post('/cartSendServer', (req, res, next) => {
    
    let userInput = JSON.stringify({
        idUser: req.body.idUser,
        via: req.body.via,
        jenisTransaksi: req.body.jenisTransaksi,
        kurirName: req.body.kurirName,
        kurirHarga: req.body.kurirHarga,
        idProvinsi: req.body.idProvinsi,
        idKota: req.body.idKota,
        idKecamatan: req.body.idKecamatan,
        detailAlamat: req.body.detailAlamat,
        pelangganNama: req.body.pelangganNama,
        pelangganWa: req.body.pelangganWa,
        pelangganMetodeKirim: req.body.pelangganMetodeKirim,
        pelangganJamDineIn: req.body.pelangganJamDineIn,
        pelangganPesanKePenjual: req.body.pelangganPesanKePenjual,
        pelangganProdukId: req.body.pelangganProdukId,
        hostId: req.body.hostId
    });
    utilSim.httpSimPost('/homepage/sendCart', userInput, req, function(result){
        if(result.rc == "00"){
            res.json({errors: false, success :true, message: result.rd})
        }else{
            res.json({errors: true, success :false, message: result.rd})
        }
    });
});

router.post('/productInquiry', utilSim.checkUserSession, (req, res, next) => {
    req.check('via', 'invalid Payment Method').not().isEmpty().withMessage('Metode pembayaran harus diisi')
    
    var errors = req.validationErrors();
    
    if(errors){
        res.json({errors: errors})
    }else{
        let userInput = JSON.stringify({
            idProduk: parseInt(req.body.idProduk),
            idCart: parseInt(req.body.idCart),
            jenisTransaksi: 2,
            via: req.body.via
        });

        utilSim.httpSimPost('/systems/transaksi/inquiry', userInput, req, function(result){
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd, data: result.data})
            }else{
                res.json({errors: true, success :false, message: result.rd})
            }
        });
    } 
});

module.exports = router;