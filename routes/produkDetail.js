const express = require('express');
const utilSim = require('../core/utilSim');
const e = require('express');
const router = express.Router();


router.get('/', (req, res, next) => {
    utilSim.httpSimGet('/homepage/productSmart/detail/' + req.query.id, req, res, function(result){
        let promo = '';
        if(result.data[0].promo == 1){
            promo = 'ribbon';
        }
        
        var path = result.data[0].image.replace("../toko/public/", "/");
        res.render('produkDetail', { title: "Detail Produk", nama: result.data[0].nama, keterangan : result.data[0].keterangan, 
        image : path, strHarga : result.data[0].strHarga, hargaCoret: result.data[0].strHargaCoret,
        id : result.data.id, harga: result.data[0].harga, promo: promo
        });
    });
});

router.get('/product', (req, res, next) => {
    let idDevel = req.query.idDevel;
    let env = req.query.env;
    
    if(undefined == req.query.env){
        env = 'prod';
    }

    if(undefined == req.query.idDevel){
        idDevel = 0;
    }

    utilSim.httpSimGet('/homepage/product?idProduk=' + req.query.id + '&idDevel=' + idDevel + '&env=' + env, req, res, function(result){
        if(result.rc == '00'){
            res.json({success :true, data: result.product})
        }else{
            res.json({success :false, errors: result.rd})
        }
    })
});


module.exports = router;
