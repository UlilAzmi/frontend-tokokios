const express = require('express');
var session = require('express-session')
const cookieParser = require('cookie-parser');
const expressValidator = require('express-validator');
const expbs = require('express-handlebars');
const app = express();
const path = require('path');
const pageRouter = require('./routes/pages');
// const pageHome = require('./routes/admin/home');
// const pagePremium = require('./routes/premium');
// const pageCart = require('./routes/cart');
const pageProdukDetail = require('./routes/produkDetail');
// const pageCarousel = require('./routes/admin/carousel');
// const pageCarouselMini = require('./routes/admin/carouselMini');
// const pageProduct = require('./routes/admin/product');
// const pageProductMember = require('./routes/admin/productMember');
// const pageProductMemberDetail = require('./routes/admin/productMemberDetail');
// const pageDashboard = require('./routes/admin/dashboard');
// const pageProfile = require('./routes/admin/profile');
// const pageNeraca = require('./routes/admin/neraca');
// const pageUsers = require('./routes/admin/users');
// const pageBeliProduk = require('./routes/admin/beliProduk');
// const pageFooter = require('./routes/admin/footer');
// const pageRole = require('./routes/admin/role');
// const pageAksesMenu = require('./routes/admin/aksesMenu');
// const pageTransaksi = require('./routes/admin/transaksi');
// const pageMutasiGift = require('./routes/admin/mutasiGift');
// const pageLogOut = require('./routes/admin/logout');
// const pageSettingWinpay = require('./routes/admin/settingWinpay');
// const pageRajaOngkir = require('./routes/admin/rajaOngkir');
const bodyParser = require('body-parser');
 


app.use(session({
    secret: 'ulil_cakep',
    resave: true,
    saveUninitialized: true
}));
app.use(cookieParser());
app.use(express.json());
app.use(expressValidator())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(express.static(path.join(__dirname, 'public')));
app.set('views', path.join(__dirname, 'views'));

var hbs = expbs.create({});
hbs.handlebars.registerHelper('isSame', function(value1, value2) {
    var hasil = false;
    if(value1 === value2){
        hasil = true;
    }
    return hasil;
  })
app.engine('handlebars', expbs({
    defaultLayout: 'main',
    layoutsDir: path.join(__dirname, 'views/layout')
}))
app.set('view engine', 'handlebars');

app.use('/', pageRouter)
// app.use('/home', pageHome)
// app.use('/cart', pageCart)
// app.use('/premium', pagePremium)
app.use('/produkDetail', pageProdukDetail)
// app.use('/carousel', pageCarousel)
// app.use('/carouselMini', pageCarouselMini)
// app.use('/productMember', pageProductMember)
// app.use('/productMemberDetail', pageProductMemberDetail)
// app.use('/beliProduk', pageBeliProduk)
// app.use('/dashboard', pageDashboard)
// app.use('/profile', pageProfile)

// app.use('/footer', pageFooter)
// app.use('/role', pageRole)

// app.use('/product', pageProduct)
// app.use('/aksesMenu', pageAksesMenu)
// app.use('/transaksi', pageTransaksi)
// app.use('/neraca', pageNeraca)
// app.use('/mutasiGift', pageMutasiGift)
// app.use('/signout', pageLogOut)
// app.use('/users', pageUsers)
// app.use('/settingWinpay', pageSettingWinpay)
// app.use('/rajaOngkir', pageRajaOngkir)

// errors: page not found 404 
app.use((req, res, next) =>{
    var err = new Error('Page not found');
    err.status = 404;
    next(err);
});

// handling error
app.use((err, req, res, next) =>{
    res.status(err.status || 500);
    // res.send(err.message);
    //console.log(req.session.menu)
    res.render('admin/page_error', {title: "Page Not Found"})
});

app.listen(8003, () => {
    console.log('Server is running on port 8003...');
});

module.exports = app;
