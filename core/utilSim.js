var http = require('http');
const fs = require('fs');
var LocalStorage = require('node-localstorage').LocalStorage;
localStorage = new LocalStorage('./scratch');
const storage = require('node-sessionstorage')
const url = 'rumahkios.com';
const port = '8001';
const api = '/api_prod';
// const api = '';
module.exports = {
    httpLogin : function(path = null, data = null, req, res){
        var options = {
            host: url,
            port: port,
            path: api+path,
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              'Content-Length': data.length
            },
          };

          var httpreq = http.request(options, function (response) {
            response.setEncoding('utf8');
            var body = "";
            response.on('data', function (chunk) {
              body += chunk;
            });
            
            req.on('error', (error) => {
              console.log("Error Login")  
              console.error(error)
            })

            response.on('end', function() {
              var jsonObject = JSON.parse(body);
                if(jsonObject.rc == "01"){
                    console.log('invalid')
                    res.render('login', {
                    title: "Login", 
                    layout: false,
                    success: false, 
                    message: 'Invalid username and password'
                    })
                }else{
                    req.session.menu = null;
                    req.session.user = null;
                    req.session.jwt = null;

                    req.session.menu = JSON.stringify(jsonObject.menu);
                    req.session.user = JSON.stringify(jsonObject.user);
                    req.session.jwt = JSON.stringify(jsonObject.jwt);
                    
                    // storage.setItem('menu', JSON.stringify(jsonObject.menu));
                    // res.cookie('jwt', jsonObject.jwt, { httpOnly: false });
                    
                    // res.render('home', {home: 1, title: "Home", menu: JSON.parse(req.session.menu), user: jsonObject.user});
                    if(jsonObject.menu.role.id == 1){
                      res.render('admin/home', {home: 1, title: "Home", menu: JSON.parse(req.session.menu), user: jsonObject.user, layout: 'admin'});
                    }else{
                      res.render('admin/myHome', {home: 1, title: "Home", menu: JSON.parse(req.session.menu), user: jsonObject.user, layout: 'admin'});
                    }
                }
          });

        });

        httpreq.write(data);
        httpreq.end()
    },

    httpSimGet : function(path = null, req, res, callback){
      let jwt = req.session.jwt;
      if(jwt == undefined){
        jwt = '';
      }else{
        jwt = JSON.parse(req.session.jwt);
      }
      
      var options = {
          host: url,
          port: port,
          path: api+path,
          method: 'GET',
          headers: {
            'Authorization' : 'Bearer ' + jwt
          },
        };

        console.log(api+path)
      var httpreq = http.get(options, function (response) {
        response.setEncoding('utf8');
        var body = "";
        response.on('data', function (chunk) {
           body += chunk;
        });
        
        req.on('error', (error) => {
          console.log("Error on Get " + error)
          return error
        })
        response.on('end', function() {
            var jsonObject = JSON.parse(body);
            if( jsonObject.rc === 'XX')
            {
              res.redirect('/login');
            }else{
              //console.log(jsonObject)
              callback(jsonObject);
            }
        });
      });
  },

  httpSimPost : function(path = null, data = null, req, callback){
    let jwt = req.session.jwt;
    if(jwt === undefined){
      jwt = '';
    }else{
      jwt = JSON.parse(req.session.jwt);
    }
    
    var options = {
        host: url,
        port: port,
        path: api+path,
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Authorization' : 'Bearer ' + jwt,
          'Content-Length': data.length
        },
      };
      
      var body = "";
      var httpreq = http.request(options, function (response) {
        response.setEncoding('utf8');
        response.on('data', function (chunk) {
            body += chunk;
        });
        
        req.on('error', (error) => {
          console.log(error)
          callback(error);
        })

        response.on('end', function() {
          if(body == "ACCEPTED"){
            callback(body);
          }else{
            var jsonObject = JSON.parse(body);
            if( jsonObject.rc === 'XX')
            {
              response.redirect('/login');
            }else{
              // response.json(jsonObject)
              callback(jsonObject);
            }
          }
        });
    });
    httpreq.write(data)
    httpreq.end()
  },

  httpSimPostDevel : function(path = null, data = null, req, callback){
    let jwt = 'eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIzIiwiZXhwIjoxNTk4ODI3OTE4LCJpYXQiOjE1OTg3OTE5MTh9.izvKfD9rGr4te0k8iKeGZglNgY3TZdSayogIDUdpfto'; 
    var options = {
        host: url,
        port: port,
        path: api+path,
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Authorization' : 'Bearer ' + jwt,
          'Content-Length': data.length
        },
      };
      
      var body = "";
      var httpreq = http.request(options, function (response) {
        response.setEncoding('utf8');
        response.on('data', function (chunk) {
            body += chunk;
        });
        
        req.on('error', (error) => {
          console.log(error)
          callback(error);
        })

        response.on('end', function() {
          if(body == "ACCEPTED"){
            callback(body);
          }else{
            var jsonObject = JSON.parse(body);
            if( jsonObject.rc === 'XX')
            {
              response.redirect('/login');
            }else{
              // response.json(jsonObject)
              callback(jsonObject);
            }
          }
        });
    });
    httpreq.write(data)
    httpreq.end()
  },

  httpSimGetDevel : function(path = null, req, res, callback){
    let jwt = 'eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIzIiwiZXhwIjoxNTk4ODI3OTE4LCJpYXQiOjE1OTg3OTE5MTh9.izvKfD9rGr4te0k8iKeGZglNgY3TZdSayogIDUdpfto';
    
    var options = {
        host: url,
        port: port,
        path: api+path,
        method: 'GET',
        headers: {
          'Authorization' : 'Bearer ' + jwt
        },
      };

      console.log(api+path)
    var httpreq = http.get(options, function (response) {
      response.setEncoding('utf8');
      var body = "";
      response.on('data', function (chunk) {
         body += chunk;
      });
      
      req.on('error', (error) => {
        console.log("Error on Get " + error)
        return error
      })
      response.on('end', function() {
          var jsonObject = JSON.parse(body);
          if( jsonObject.rc === 'XX')
          {
            res.redirect('/login');
          }else{
            //console.log(jsonObject)
            callback(jsonObject);
          }
      });
    });
},

  

httpSimDelete : function(path = null, req, callback){
  let jwt = req.session.jwt;
  if(jwt === undefined){
    jwt = '';
  }else{
    jwt = JSON.parse(req.session.jwt);
  }

  var options = {
      host: url,
      port: port,
      path: api+path,
      method: 'DELETE',
      headers: {
        'Authorization' : 'Bearer ' + jwt
      },
    };

    var httpreq = http.get(options, function (response) {
      response.setEncoding('utf8');
      response.on('data', function (chunk) {
          var jsonObject = JSON.parse(chunk);
          if( jsonObject.rc === 'XX'){
            res.redirect('/login');
          }else{
            callback(jsonObject);
          }
      });
      
      req.on('error', (error) => {
        console.log(error)
        return error
      })
  });
  httpreq.end()
},

  httpSimPut : function(path = null, data = null, req, callback){
    let jwt = req.session.jwt;
    if(jwt === undefined){
      jwt = '';
    }else{
      jwt = JSON.parse(req.session.jwt);
    }

    var options = {
        host: url,
        port: port,
        path: api+path,
        method: 'PUT',
        headers: {
          'Authorization' : 'Bearer ' + jwt,
          'Content-Type': 'application/json',
          'Content-Length': data.length
        },
      };
      var httpreq = http.request(options, function (response) {
        response.setEncoding('utf8');
        response.on('data', function (chunk) {
            var jsonObject = JSON.parse(chunk);
            if( jsonObject.rc === 'XX')
            {
              res.redirect('/login');
            }else{
              callback(jsonObject);
            }
        });
        
        req.on('error', (error) => {
          console.log(error)
          return error
        })
    });
    httpreq.write(data)
    httpreq.end()
  },

  checkUserSession : function( req, res, next )
  {
      // if( req.cookies.jwt && null != req.session.menu )
      // {
      //     next();
      // }
      // else
      // {
      //     res.redirect('/login');
      // }
      next();
  },

  getMenu :  function(req, res, next){
    return req.session.menu;
  },

  getMenuDevel :  function(req, res, next){
    // fs.readFile('public/menu.json', (err, data) => {
    //   if (err) throw err;
    //   let menu = JSON.parse(data);
    //   // req.session.menu = data;
    //   // req.session.user = JSON.stringify(jsonObject.user);
    //   // console.log(menu);
      
    // });
    return fs.readFileSync('public/menu.json', 'utf8');
  },
  
  getUserDevel : function(callback){
    return fs.readFileSync('public/user.json', 'utf8');

    //  await fs.readFileSync('public/user.json', (err, data) => {
  //     if (err) throw err;
  //     let user = JSON.parse(data);
  //     console.log(user);
  //     return callback(user);
  //   });
  }
}

var utilSim = function () {
}
